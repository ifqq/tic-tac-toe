function checkGameState(gameField) {
  // проверка по строкам
  for (let i = 0; i < gameField.length; i++) {
    if (gameField[i][0] === gameField[i][1] && gameField[i][1] === gameField[i][2]) {
      if (gameField[i][0] === "x") {
        return "Крестики победили";
      } else if (gameField[i][0] === "o") {
        return "Нолики победили";
      }
    }
  }

  // проверка по столбцам
  for (let i = 0; i < gameField.length; i++) {
    if (gameField[0][i] === gameField[1][i] && gameField[1][i] === gameField[2][i]) {
      if (gameField[0][i] === "x") {
        return "Крестики победили";
      } else if (gameField[0][i] === "o") {
        return "Нолики победили";
      }
    }
  }

  // проверка по диагоналям
  if (gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2]) {
    if (gameField[0][0] === "x") {
      return "Крестики победили";
    } else if (gameField[0][0] === "o") {
      return "Нолики победили";
    }
  }
  if (gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0]) {
    if (gameField[0][2] === "x") {
      return "Крестики победили";
    } else if (gameField[0][2] === "o") {
      return "Нолики победили";
    }
  }

  // проверка на ничью
  for (let i = 0; i < gameField.length; i++) {
    for (let j = 0; j < gameField[i].length; j++) {
      if (gameField[i][j] === null) {
        return "Игра не завершена";
      }
    }
  }
  return "Ничья";
}

const gameField = [
  ["x", "o", "x"],
  ["o", "o", "x"],
  ["x", "x", "o"],
];
